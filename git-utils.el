;;; git-utils.el --- Git utility functions

;; Copyright (C) 2021 James Wykeham

;; Author: James Wykeham <jwykeham@gmail.com>
;; URL: https://gitlab.com/jwykeham/git-utils
;; Version: 1.0.0
;; Keywords: vc

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Git utilities
;;
;; See documentation on https://gitlab.com/jwykeham/git-utils

;;; Code:

(require 's)

(defvar git-utils--time-format "%F %R" "Format for timestring from git procelain.")

(defvar git-utils--blame-format "${author-time} <${author}> - ${summary}" "Blame format.")

(defun git-utils--command-process-filter (proc string)
  "Process filter."
  (with-current-buffer (get-buffer-create (format "*%s*" (process-name proc)))
    (erase-buffer)
    (insert string)
    (display-buffer (current-buffer))))

(defun git-utils--blame-process-list (key value)
  "Key value processing to con."
  (when (string-match-p (regexp-quote "time") key)
    (setq value (format-time-string git-utils--time-format (seconds-to-time (string-to-number value)))))
  (cons key value))

(defun git-utils--blame-process-filter (proc string)
  "Git blame process-filter."
  (message (s-format git-utils--blame-format 'aget (mapcar (lambda (x) (git-utils--blame-process-list (car x) (car (cdr x)))) (mapcar 'cdr (s-match-strings-all "^\\([a-z\\-]+\\)\\s-\\(.+\\)$" string))))))

(defun git-utils-line-history ()
  "Show current line hostory."
  (interactive)
  (set-process-filter
   (start-process "emacs-git-line-history" nil "git" "--no-pager" "log" "-p" "-L" (format "%d,+1:%s" (+ 1 (current-line)) (buffer-file-name (current-buffer))))
   'git-utils--command-process-filter))

(defun git-utils-blame ()
  "Git blame current line."
  (interactive)
  (set-process-filter
   (start-process "emacs-git-line-blame" nil "git" "--no-pager" "blame" "-p" "-L" (format "%d,+1" (+ 1 (current-line))) (buffer-file-name (current-buffer)))
   'git-utils--blame-process-filter))

(provide 'git-utils)
;;; git-utils.el ends here
